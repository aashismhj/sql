# Basic DML
## Insert
```sql
insert into db_user (username, password, email, join_date, content) 
values 
('user 1', 'password', 'email@email1.com', '2024-10-10', 'lorem'),
('user 2', 'password', 'email@email2.com', '2024-10-10', 'lorem'),
('user 3', 'password', 'email@email3.com', '2024-10-10', 'lorem'),
('user 4', 'password', 'email@email4.com', '2024-10-10', 'lorem'),
('user 5', 'password', 'email@email5.com', '2024-10-10', 'lorem')
;
insert into user_address (user_id, country, city) 
values
(5, 'Canada', 'big-ilish'),
(6, 'Bromon', 'Kantos'),
(7, 'Cromon', 'Gotham');
```
### Upsert
```sql
insert into db_user(username, password, email, join_date, content) values ('user 5', 'password', 'email@email5.com', '1999-01-01','lorem updated') on conflict on constraint db_user_username_key do update set join_date = EXCLUDED.join_date, content = EXCLUDED.content;
```
On conflict can be used for various purpose, we can perform different operation for different conflict

### Update Join
We can use `UPDATE` to update data in a table based on values in another table.
```sql
UPDATE product
SET net_price = price - price * discount
FROM product_segment
WHERE product.segment_id = product_segment.id;
```
### returning
```sql
insert into db_user (username, password, email, join_date, content) 
values 
('user 1', 'password', 'email@email1.com', '2024-10-10', 'lorem') returning *;
```