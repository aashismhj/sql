```sql
SELECT 10.0 / 3.0;
select * from (values('some', 'this'))  as t(col1, col2);
-- 
BEGIN TRANSACTION
   DECLARE @DataID int;
   INSERT INTO DataTable (Column1 ...) VALUES (....);
   SELECT @DataID = scope_identity();
   INSERT INTO LinkTable VALUES (@ObjectID, @DataID);
COMMIT
```
**Duplicating Rows**
```sql
-- duplicate rows
Do $$
DECLARE
	inc INT := 0;
BEGIN
	FOR i IN 1..999 LOOP
		INSERT INTO log_data (create_at, created_time, updated_at, updated_time, country, time_taken, ref_code) 
		select create_at, created_time, updated_at, updated_time, country, time_taken, ref_code from log_data  order by create_at limit 1000;
		inc := inc + 1;
		RAISE NOTICE 'Inserting duplicate number %', inc;
	END LOOP;
	
END;
$$;
```
**Inserting random values**
```sql
-- Insert a large number of rows
INSERT INTO example_table (name, age)
SELECT md5(random()::text), (random() * 100)::int
FROM generate_series(1, 1000000);
```
```bash
# postgres show null as null instead of empty string
\pset null null
```
**Group Concat**
```sql
SELECT 
    student_name,
    GROUP_CONCAT(student_subject ORDER BY student_subject ASC) AS subjects
FROM 
    student
GROUP BY 
    student_name;

```

## To Know
- Multi Version COncurrency COntrol