# Basic Query (DQL)
## Querying Data
```sql
-- basic select and order by
select country as order_by, * from customers order by country;
-- multiple order by
select country as order_by, * from customers order by country desc, customer_id asc;
-- by default null are shown first, if you want to show null first
SELECT region FROM customers ORDER BY region DESC NULLS FIRST;
- basic distinct
select distinct(contact_title) as distinct_title from customers;
-- count no of distinct 
select distinct contact_title as distinct_title, company_name from customers;
-- counting distinct contact_title with their count
select contact_title, count(*) from customers group by contact_title; 
-- using operation in select
select unit_price, quantity, (unit_price * quantity) as amount from order_details;
```

## Filtering Data
```sql
select * from order_details where product_id = 11 and quantity = 12;
select * from order_details where (product_id = 11 or product_id = 12) and quantity = 12;

-- selecting double precision value
select * from order_details where unit_price between 9 and 9.9;
-- limit and offset
select * from suppliers limit 2 offset 2;

-- wild cards
select * from suppliers where country like '_S_';
select * from suppliers where country like 'Ja%n';

-- in
select * from suppliers where city in ('London', 'New Orleans');
-- case insensitive
select * from suppliers where LOWER(city) = 'london';
```
**Fetch**
[res](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-fetch/)
```sql
select * from orders order by order_id fetch first 5 row only;
```
## groping
### group by
The `GROUP BY` clause divides the rows returned from the `SELECT` statement into groups. We can't use where to filter, to filter the group by because `GROUP BY` runs after where;
```sql
-- this will throw error, when using group by the column to show must be the group by column or in a aggregate function
select * from customers group by country;
select country, company_name from customers group by country, company_name;
-- we mainly use group by with aggregate functions
select product_id, count(*) from order_details group by (product_id);
-- you must understand the query execution order 
select unit_price, unit_price || 'koko' from order_details group by unit_price;

--
select product_id, sum(quantity) from order_details group by (product_id);
```
### HAVING
The `HAVING` clause specifies a search condition for a group. The `HAVING` clause is often used with `GROUP BY` clause based on a specified condition. 
```sql
select unit_price, unit_price || 'koko' from order_details group by unit_price having unit_price > 28.5;
select product_id, sum(quantity) as the_sum from order_details group by (product_id) having the_sum > 100;
```
## Join
*Important*

### Left Join
```sql
insert into employees(employee_id,last_name, first_name, title, title_of_courtesy, birth_date, hire_date, address, city) values (101,'Cunning', 'Ham', 'Saviour of Relms', 'Duke', '1880-10-10', '1990-09-09', 'New England', 'E City' );
--
select emp.employee_id, first_name, last_name, order_date from employees emp
left join orders ord on emp.employee_id = ord.employee_id
where order_date is null;
```
### Inner join
```sql
select emp.employee_id, first_name, last_name, order_date from employees emp inner join orders ord on emp.employee_id = ord.employee_id where order_date is null;
```
### Self Join
### Full outer join
```sql
-- multi join
select orders.order_id, ship_address, customer_id, orders.employee_id, unit_price, quantity, first_name, last_name
from orders
left join order_details on orders.order_id = order_details.order_id
left join employees on employees.employee_id = orders.employee_id;
```

## Grouping
The `GROUP BY` clause divides the rows returned from the `SELECT` statement into groups. For each group, we can apply aggregate function such as `sum()` to calculate the sum of items or `count()` to get the number of items in the groups.
## Set Operations
```sql
CREATE TABLE top_rated_films(
  title VARCHAR NOT NULL, 
  release_year SMALLINT
);

CREATE TABLE most_popular_films(
  title VARCHAR NOT NULL, 
  release_year SMALLINT
);

INSERT INTO top_rated_films(title, release_year) 
VALUES 
   ('The Shawshank Redemption', 1994), 
   ('The Godfather', 1972), 
   ('The Dark Knight', 2008),
   ('12 Angry Men', 1957);

INSERT INTO most_popular_films(title, release_year) 
VALUES 
  ('An American Pickle', 2020), 
  ('The Godfather', 1972), 
  ('The Dark Knight', 2008),
  ('Greyhound', 2020);
```

### Union
Union operator allows us to combine the result sets of two or more SELECT statement into a single result set.
```sql
-- syntax
SELECT select_list from A UNION select select_list from B;
--
SELECT * FROM top_rated_films UNION SELECT * FROM most_popular_films; 
SELECT * FROM top_rated_films UNION ALL SELECT * FROM most_popular_films; -- union shows all rows also repeating the duplicates
```
### Intersect
The INTERSECT operator returns a result set containing rows that are available in both result sets.
```sql
-- syntax
SELECT select_list FROM A INTERSECT SELECT select_list FROM B;
SELECT * FROM most_popular_films INTERSECT SELECT * FROM top_rated_films;
```

### Except
EXCEPT operator returns distinct rows from the first (left) query that re not in the second (right) query
```sql
SELECT select_list FROM A EXCEPT SELECT select_list FROM B ORDER BY sort_expression;
SELECT * FROM top_rated_films EXCEPT SELECT * FROM most_popular_films;
```
**Set vs Join**
- Join operations are used to combine rows from different tables based on related columns, while SET operations are used to combine and manipulate the results of queries.
- JOIN operations work with tables, while SET operations work with the results of queries.
- JOIN operations return rows from the input table while SET operations return rows from the result SET operations return rows from the result sets of queries.
- JOIN operations do not affect duplicates, while SET operations may remove duplication depending on the operation (UNION removes duplicates, UNION ALL includes duplicates)
- JOIN operations typically involves more processing can can be slower then SET operations, especially for large datasets.
- Use JOIN operations when we need to retrieve related data from different tables based on a common column.
- USE SET operations when we need to combine the results of multiple queries, find the intersection or difference between result sets, or remove duplicates from the combined result set.
