# Postgres Inbuilt functions
*(might be different for other db engines)*
1. Mathematical Functions
- ABS()
- CEIL(), FLOOR()
- ROUND()
- POWER()
- SQRT()
2. String Functions
- CONCAT()
- SUBSTRING()
- LENGTH(), CHAR_LENGTH()
- UPPER(), LOWER()
- TRIM(), LTRIM(), RTRIM()

3. Date and Time Functions
- NOW()
- DATE_TRUNC()
- EXTRACT()
- AGE()
- TO_TIMESTAMP(), TO_DATE(), TO_CHAR()

4. Aggregate Functions
- SUM(), AVG(), COUNT(), MIN(), MAX()
- GROUP_CONCAT()
5. Conditional Functions
- CASE
- COLESCE
6. JSON Functions
- JSONB_INSERT(), JSONB_DELETE()
- JSONB_ARRAY_ELEMENTS()

7. Windows Functions
- ROW_NUMBER(), RANK(), DENSE_RANK()

**Res**
1. (google)[https://cloud.google.com/spanner/docs/reference/postgresql/functions-and-operators]