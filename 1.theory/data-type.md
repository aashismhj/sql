# Data Types (in Postgres)
## [Interval](https://www.javatpoint.com/postgresql-interval)
The Interval a data type used to store and deploy Time in years, months, days, hours, minutes, seconds, etc. And the months and days values are integers values, whereas the second's field can be fractions values.
PostgresSQL interval data type value involves 16 bytes storage size, which help helps to store a period with the acceptable range from -178000000 years to 178000000 years.

```sql
-- syntax
@ interval [field] [(p)]
```
field: The field parameter is used to show the Time
p: p is used to display precision value
@: We can ignore the @ parameter as it is an optional parameter

```sql
@interval '6 months before';
@interval '2 hours 30 minutes';
```

## blob/bytea