# View and Material Views

## View
A view is a virtual table that is based on the result set of a SQL query. It does not store dta physically but dynamically generates data from the underlying table when queried. Views are useful for encapsulating complex queries, simplifying data access, enhancing security, and providing a level of abstraction.

### Key Features
- Virtual Table: A Table behaves like a table in SQL queries but does not store data physically.
- Encapsulation: Encapsulates complex queries into a single object, making it easier to reuse and maintain.
- Security: Can restrict access to specific data by exposing only a subset of the columns and rows from the underlying tables.
- Simplification: Simplifies access to frequently used joins, aggregations, and calculations.


## Materialized Views
A material view actually runs the query and stores the results. In essence, this means the materialized view acts as a cache for the query. Material view can create indexes on them because, under the hood, they're just table that store the results of query.


## Key Features
- Persistent Storage: The result set of the query is stored on disk, which allows for faster query performances since t doesn't need to recompute the results each time.
- Refreshable: 
- Indexing: We can create indexes on material views to further enhance performance.

### Usage
- Data aggregations
- Frequent reports
- Rollups (or reducing data granularity): Materialized views will also help us when we're collecting data by second but would like to create views by minute or by hour.
- Read-heavy operations.
- Analysis of historical data: Material views can help us store and quickly access historical data snapshots for trend analysis.
- Data warehousing and BI