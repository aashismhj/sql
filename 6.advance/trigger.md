# Trigger
A trigger is a function invoked automatically whenever an event associated with a table occurs. An event could be INSET, DELETE or TRUNCATE.
A trigger is a special user-defined function associated with a table. To create a new trigger, we define a trigger function first, and then bind this trigger function to a table.
**Postgres trigger types**
- Row Level triggers
- Statement level trigger
## Basic Trigger
## INSERT Trigger
## UPDATE Trigger
## UPDATE Trigger
## DELETE Trigger
## INSTEAD OF Trigger
## TRUNCATE trigger
## Enabling and Disabling triggers
## Listing triggers
## Advance triggers
1. Event Trigger
2. Conditional Trigger
## RES
1. https://www.postgresqltutorial.com/postgresql-triggers/