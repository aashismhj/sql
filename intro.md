# SQL 

## Basic Theory 
1. Relation Database
2. [SQL command types](https://www.geeksforgeeks.org/sql-ddl-dql-dml-dcl-tcl-commands/)
3. data types: 
    - basic
    - uuid
    - date time
    - interval
    - array
    - json
    - enum
    - hstore
    - blob/bytea
    - special data types: box, line, point, lesg, polygon, inet, macaddr
4. schema design
    - normilization
    - db relation
        - one to one
        - one to many
        - many to many
    - constraints
5. [Query Execution](https://postgrespro.com/blog/pgsql/5969262)

## Basic DDL
### Managing Tables
1. Data Types
2. Create table
3. alter table
    - add column
    - change column name
    - change data type
    - add/remove constraint
4. select into and create table as
5. sequence and auto increment
    - sequence start value and increment
    - cycle
6. identity column
7. rename table
8. drop table
    - drop table
    - drop table with constraint
9. [temporary table](https://www.freecodecamp.org/news/sql-temp-table-how-to-create-a-temporary-sql-table/)

### constraints
1. primary key
2. foreign key
3. check constraint
4. unique constraint
5. not null constraint
6. default constraint
7. delete cascade
    - CASCADE
    - SET NULL
    - SET DEFAULT
    - RESTRICT
    - NO ACTION

## Basic DML
1. insert, upsert
3. update
3. delete, truncate
4. returning

## Basic Query (DQL)
### Querying Data
1. Select
    - select into
2. column aliases
3. Order by
    - order by
    - order by multiple column
    - order by null
    - order by custom (order by filed:mysql)
4. Select Distinct

### Filtering Data
1. where
2. binary operator: AND, OR
3. limit, offset, fetch
4. like, in , between, is null
5. select top: is used to specify the number of records to return. 
6. [wild cards](https://www.programiz.com/sql/wildcards)
*Not all database systems support the select top. MySQL supports the LIMIT clause to select a limited number of records, while Oracle uses FETCH FIRST n ROWS ONLY and ROWNUM.

### [Joining Tables](https://www.geeksforgeeks.org/sql-join-set-1-inner-left-right-and-full-joins/)
1. join
2. table aliases
3. inner join
4. left join
5. self join
6. full outer join
7. cross join
8. natural join
*USING()*

### grouping
- group by
- having

### Set Operations
- union
- intersect
- expect

### Grouping sets, Cubes, and Rollups
- Groping sets
- cube 
- rollup

### [Aggregate functions](https://www.tutorialspoint.com/postgresql/postgresql_useful_functions.htm)
- count
- max
- min
- avg
- sum 
- array
- numeric

### [in-built functions](./4.basic-query/postgres-inbuild-functions.md)
- lower()
- upper()
- initcap()
- substring()
- length()
- replace()
- repeat()
- sqrt()
- round()
- extract()
- date_trunc()
https://www.postgresql.org/docs/current/functions-datetime.html
https://www.postgresql.org/docs/current/functions-aggregate.html
https://www.postgresql.org/docs/current/functions-array.html
https://www.postgresql.org/docs/current/functions-json.html
https://www.postgresql.org/docs/current/functions-string.html

## Advance Querying
### [Pattern Matching Operators](./5.advance-querying/pattern-matching-operators.md)
### Subquery
### Common Table Expressions
1. CTE
2. Recursive CTE
*Temporary table vs CTE*

### window functions

### Condition
1. CASE
2. COALESCE
3. NULLIF
4. CAST


## Best Pratice