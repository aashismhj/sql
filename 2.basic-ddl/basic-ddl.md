# Basic Query
## Managing Tables
### Create Table
```sql
-- basic syntax
CREATE TABLE [IF NOT EXISTS] table_name (
   column1 datatype(length) column_constraint,
   column2 datatype(length) column_constraint,
   ...
   table_constraints
);
-- create table with primary key, default values, and different data types
CREATE TABLE db_user (
  user_id SERIAL PRIMARY KEY, 
  username VARCHAR (50) UNIQUE NOT NULL, 
  password VARCHAR (50) NOT NULL, 
  email VARCHAR (255) UNIQUE NOT NULL, 
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  join_date DATE NOT NULL,
  content TEXT default null, 
  last_login TIMESTAMP
);
-- where to put foreign key
-- where to put foreign key here?
CREATE TABLE user_address (
    address_id INT GENERATED ALWAYS AS IDENTITY,
    user_id int,
    country VARCHAR(100) NOT NULL,
    city VARCHAR(100) NOT NULL,
    PRIMARY KEY(address_id),
    CONSTRAINT fk_user_address FOREIGN KEY(user_id) REFERENCES db_user(user_id)
);
-- create unique index
create unique index idex_city_country on user_address(city,country);
```
**Primary key vs Unique**
- there can only be one primary key 
- primary key can not be null while unique can
- primary key are use as the main identifier
### Alter table
```sql
CREATE TABLE temp_for_alter (
  name varchar(300)
);
-- add column 
ALTER TABLE temp_for_alter 
  ADD column age int default 0,
  ADD COLUMN table_id SERIAL not null,
  ADD CONSTRAINT table_pk PRIMARY KEY(table_id);
-- alter column change data type
-- type conversion 
alter table temp_for_alter alter column age type varchar(10);
-- will show error because 
alter table temp_for_alter alter column age type int;
-- correct way to do
-- postgres: alter column, with also converting values of a column to the new ones
alter table table_name alter column_name type new_data_type using expression;
SELECT * FROM temp_for_alter WHERE age !~ '^\d+$';
ALTER TABLE temp_for_alter ALTER COLUMN age TYPE integer USING age::integer;
-- rename column
ALTER TABLE temp_for_alter RENAME COLUMN age to age_modified;
-- drop column
ALTER TABLE temp_for_alter DROP COLUMN age_modified;

```
### Identity column
An Identity column is a special type of column in a relational database that automatically generates unique values for each new row inserted into a table. This particularly useful for primary keys, as it ensures that each row has a unique identifier without requiring manual input. The values are typically generated sequentially, starting from a specified initial value and incrementing by a specified step.
### Auto Increment and Sequence
Sequence is a database object that generates a sequence of unique numbers ina specific order. Sequence are independent of tables and can be used for various purposes beyond primary keys, such as generating unique transaction ids or other unique identifiers.
```sql
-- create sequence
CREATE SEQUENCE my_sequence start 2 increment 3;
-- 
select last_value from my_sequence;
insert into temp_for_alter (table_id,name) values( nextval('my_sequence'), 'name 1'), (nextval('my_sequence'), 'name 2');
alter table temp_for_alter add column column_one int default nextval('my_sequence') ,add column column_two int default nextval('my_sequence');

-- create sequence with rotation, min and max value
CREATE SEQUENCE my_sequence
START WITH 1 INCREMENT BY 1 MINVALUE 1 MAXVALUE 100 CYCLE;
-- # auto increment
-- Using serial (old method)
CREATE TABLE my_table (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100)
);

-- Using identity (ANSI SQL standard)
CREATE TABLE my_table (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    name VARCHAR(100)
);
```
### Change table name
```sql
ALTER TABLE temp_for_alter RENAME TO drop_me;
```
### 
```sql
DROP TABLE drop_me;
```
### create table as
The 'CREATE TABLE AS' statement creates a new table and fills it with the data returned by a query. 
```sql
-- basic syntax
CREATE TEMP TABLE new_table_name as query;
-- create temporary table 
CREATE TEMP TABLE new_table_name as query;
-- create unlogged table
CREATE UNLOGGED TABLE new_table_name as query;
-- create table as
create table my_temp as select username, password, city from db_user left join user_address on user_address.user_id = db_user.user_id;
--
```
```sql
--- # Temporary table
-- Create a temporary table with data subset
CREATE TEMPORARY TABLE subset_data AS
SELECT column1, column2, column3
FROM original_table
WHERE condition;

-- Perform analysis on the subset data
SELECT column1, AVG(column2) AS average_value
FROM subset_data
GROUP BY column1;

-- Drop the temporary table
DROP TABLE subset_data;
```


## Constraints
Constraints in a database are rules enforced on data columns to ensure the integrity, accuracy, and reliability of the data. They help maintain data consistency and prevent invalid data entries.
### Primary Key
- Ensures that each row in a table has a unique identifier.
- Enforce uniqueness and does not allow null values.
- There can be only one primary key per table, which can consist of one or more columns
```sql
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50)
);
```
### Foreign Key
- Enforces a link between the data in two tables.
- Ensures that the value in one table matches a value in another table.
- Ensures the the value in one table matches a value in another table, maintaining referential integrity.
- Can also enforce cascading actions like 'ON DELETE CASCADE' or 'ON UPDATE CASCADE'
```sql
CREATE TABLE orders (
    order_id INT PRIMARY KEY,
    employee_id INT,
    FOREIGN KEY (employee_id) REFERENCES employees(employee_id)
);
```
### Unique Constraint
- Ensure that that all values in column or a group or a group of columns are unique across the table.
- Allows are single null value in the constrained columns (with some database specific variations)
```sql
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    email VARCHAR(100) UNIQUE
);
```
### Default Constraint
- Sets a default value
```sql
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    start_date DATE DEFAULT CURRENT_DATE
);
```
### Check Constraint
Ensure that the values in a column meet a specific condition. It can be used for various such as value ranges, patterns, etc.
```sql
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    age INT,
    CHECK (age >= 18 AND age <= 65)
);
```
### Not Null Constraint
```sql
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL
);
```
### Composite Constraints
- Constraints can span multiple columns
```sql
CREATE TABLE employee_projects (
    employee_id INT,
    project_id INT,
    PRIMARY KEY (employee_id, project_id)
);
```
### Deferrable Constraints
Some database support deferrable constraints, which can be temporarily disabled within a transaction to allow certain operations that would otherwise violate the constraint
```sql
-- PostgreSQL example
CREATE TABLE employees (
    employee_id INT PRIMARY KEY,
    email VARCHAR(100) UNIQUE DEFERRABLE INITIALLY DEFERRED
);
```
### Delete Constraint
1. CASCADE
When a referenced row in the parent table is deleted, all related rows in the child table are also deleted. This is useful for maintaining consistency when the deletion of a parent record necessitates the removal of related child orders.
```sql
CREATE TABLE orders (
    order_id INT PRIMARY KEY,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
    ON DELETE CASCADE
);
```
2. SET NULL
When a referenced row in the parent table is deleted, the foreign key columns in the related rows of the child table are set to NULL. This is useful when the relationship between the parent and child records is no longer valid, but the records should remain in the database.
```sql
CREATE TABLE orders (
    order_id INT PRIMARY KEY,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
    ON DELETE SET NULL
);
```
3. SET DEFAULT
When a referenced row in the parent table is deleted, the foreign key columns in the related rows of the child table are set to their default values. This requires that the foreign key columns have a default value defined.
```sql
CREATE TABLE orders (
    order_id INT PRIMARY KEY,
    customer_id INT DEFAULT 0,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
    ON DELETE SET DEFAULT
);
```
4. RESTRICT
Prevents the deletion of a referenced row in the parent table if there are related rows in the child table. This is useful for ensuring that the parent record is not deleted while child records still exist.
```sql
CREATE TABLE orders (
    order_id INT PRIMARY KEY,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
    ON DELETE RESTRICT
);
```

5. NO ACTION
Similar to 'RESTRICT', but the check is deferred until the end of the transaction. If we try to delete a parent row that has related child rows, an error is raised to the end of the transaction. This is the default behavior if no delete action is specified.
```sql
CREATE TABLE orders (
    order_id INT PRIMARY KEY,
    customer_id INT,
    FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
    ON DELETE NO ACTION
);
```
*How to set default value based on separate column*