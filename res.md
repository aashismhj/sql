# SQL Resources
1. [postgresqltutorial.com](https://www.postgresqltutorial.com/)
1. [mysqltutorial.com](https://www.mysqltutorial.org/)
1. [datacamp.com](https://www.datacamp.com/)
1. [sqlservertutorial.net](https://www.sqlservertutorial.net/)
1. [pganalyze](https://pganalyze.com/docs/)
## Sample Databases
1. [northwind for psotgres](https://github.com/pthom/northwind_psql)
1. [dvdrental](https://www.postgresqltutorial.com/postgresql-getting-started/postgresql-sample-database/)

## Games
1. [sql murder mystery](https://mystery.knightlab.com/)
1. [sql island](http://wwwlgis.informatik.uni-kl.de/extra/game/?lang=en)
1. [sql police department](https://sqlpd.com/)
1. [schemaverse](https://schemaverse.com/)


1. [dbdiagram](https://dbdiagram.io/d): Draw db schema 