# Advance Topic
## [Indexes](./6.advance/indexes.md)
## Schema
## [View and Material View](https://www.timescale.com/learn/guide-to-postgresql-views)
    - material view indexes
    - view trigger and rules
## Functions
- name notion
## Stored Procedures
https://www.shiksha.com/online-courses/articles/stored-procedure-vs-function-what-are-the-differences
https://www.enterprisedb.com/postgres-tutorials/10-examples-postgresql-stored-procedures
- call
- perform
- execute
We cannot CALL a function.
We cannot SELECT or PERFORM a procedure;
We cannot PERFORM an EXECUTE, or EXECUTE a PERFORM, neither being an SQL command
## Cursor
## Full Text Search
## Trigger
## Partation
## Sharding
## Locks
## Backup and snapshot, import and export data
## Extensions
## Schedule
## Explain Analyze
```sql
explain ( analyze true, format JSON ) select * from log_data limit 1;
```

## Misc
1. query execution order
1. Vacuum
1. [ANSI SQL standard](https://dwbi.org/pages/30)
1. query plan vs execution plan
1. collations
1. operators
    - :=