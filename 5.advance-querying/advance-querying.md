# Advance Querying

## [SubQuery](./subquery.md)

## Condition
```sql
SELECT *
FROM your_table
ORDER BY
  CASE 
    WHEN position = 'president' THEN 1
    WHEN position = 'vice-president' THEN 2
    ELSE 3 -- Assuming 'member' is the lowest position
  END;
```