# Pattern matching operators (Postgres)
## Regular Expression Operators
1. Match: ~
- Checks if a string matches a regular expression pattern.
```sql
select * from table where column ~ 'pattern';
```
2. Not Match: !~
- Checks if a string does not match a regular expression pattern.

3. Case-Insensitive: ~*
- Checks if a string matches a regular expression pattern, case-insensitivity

4. Case-Insensitive Not Match: !~*

## LIKE Operators
1. LIKE
- Checks if a string matches a pattern using SQL simple pattern
2. NOT LIKE
- Checks if a string does not match a pattern using SQL's simple pattern matching
3. ILIKE
- Checks if a string matches a pattern, case-insensitivity

4. NOT ILIKE
- Checks if a string does not match a pattern, case-insensitivity

## POSIX Regular Expression Operators
1. SIMILAR TO
- Check if a string matches a SQL standard regular expression pattern
```sql
select * from users where name similar to '(john|jane)%';
```
2. NOT SIMILAR TO
- Checks if a string does not matches a SQL standard regular expression pattern.

**The operators and pattern matching techniques described for PostgresSQL can vary significantly between different database management systems. MYSql uses REGEX, Oracle uses REGEXP_LIKE()**
**Res**
1. https://towardsdatascience.com/pattern-matching-and-regular-expressions-in-postgresql-cd1fa76e5f3f