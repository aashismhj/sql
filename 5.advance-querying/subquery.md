# SubQuery
A sub query is a query within another query. It is also known as an inner or nested query.A sub query can be useful for retrieving data that will be used by the main query as a condition for further data selection.

- Sub query can be used in two ways appending into the select, or filtering the select

Sub query provide a number of benifits like 
- It helps write readable code. It helps break complex queries into simpler parts, making the main query easier to read and understand. 
- We can avoid joining large tables multiple times, redundancy improving query performance in certain scenarios.
- It can be used to dynamically filter or aggregate data based on the results of another query, allowing for more flexible and powerful data retrieval options.

```sql
-- in select clause
SELECT p.product_id, p.product_name, (SELECT SUM(od.quantity) FROM order_details od WHERE od.product_id = p.product_id)total_quantity_ordered FROM products p;
-- in from clause
SELECT e.employee_id, e.last_name, e.first_name, total_sales
FROM employees e
JOIN (SELECT o.employee_id, SUM(od.unit_price * od.quantity) AS total_sales
      FROM orders o
      JOIN order_details od ON o.order_id = od.order_id
      GROUP BY o.employee_id) AS sales
ON e.employee_id = sales.employee_id;
-- in where clause
SELECT o.order_id, o.order_date, o.customer_id
FROM orders o
WHERE (SELECT SUM(od.unit_price * od.quantity)
       FROM order_details od
       WHERE od.order_id = o.order_id) > 1000;
```
### Correlated Subquery
Show us how to use a correlated subquery to perform a query that depends on the values of the current row being processed.
```sql
SELECT c.customer_id, c.company_name FROM customers c WHERE (SELECT COUNT(*) FROM orders o WHERE o.customer_id = c.customer_id) > 10;
```
### ANY
Retrieve data by comparing a value with a set of values returned by a subquery.
```sql
-- to test
SELECT productname, unitprice
FROM products
WHERE unitprice > ANY (
    SELECT unitprice
    FROM products
    WHERE categoryid = (SELECT categoryid FROM categories WHERE categoryname = 'Beverages')
);
```
### ALL
Query data by comparing a value with a list of values returned by a subquery.
```sql
-- to test
SELECT e.employeeid, e.firstname, e.lastname, e.salary
FROM employees e
WHERE e.salary > ALL (
    SELECT salary
    FROM employees
    WHERE departmentid = (SELECT departmentid FROM departments WHERE departmentname = 'Sales')
);
```
### EXISTS
check for the existence of rows returned by a subquery.
```sql
-- to test
SELECT *
FROM Customers
WHERE EXISTS (
    SELECT *
    FROM Orders
    WHERE Orders.CustomerID = Customers.CustomerID
);
```

**Some Cons of Subquery**
- They can lead to significant performance overhead since the subquery might be executed multiple times, espically for corrrelated subqueries.
- For large datasets, subqueries can become inefficient compared to equavalent joins or window functions