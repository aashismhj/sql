# Window Function

Similar to an aggregate function, a window function operates on a set of rows. However, it does not reduce the number of rows returned by the query.
The term windows describes the set of rows on which the windows function operates. A window function returns values from the rows in a window.

**PostgresSQL Window Function Syntax**
```sql
window_function(arg1, arg2,..) OVER (
   [PARTITION BY partition_expression]
   [ORDER BY sort_expression [ASC | DESC] [NULLS {FIRST | LAST }])  
```

In this syntax: window_function(arg1, arg2)
The window_function is the name of the window function. Some window functions do not accept any argument.

PARTITION BY clause
The PARTITION BY clause divides rows into multiple groups or partitions to which the window function is applied. The partition by clause is options. if we skip it the window function will treat the whole result set as a single partition.

ORDER BY clause
The ORDER BY clause specifies the order of rows in each partition to which the window function is applied. 

The ORDER BY clause uses the NULLS FIRST or NULLS LAST option to specify whether nullable values should be first or last in the result set. The default is NULLS LAST option.

frame_clause
The frame_clause defines a subset of rows in the current partition to which the window function is applied. This subset of rows is called a frame. If we use multiple window functions in a query:
```sql
SELECT
    wf1() OVER(PARTITION BY c1 ORDER BY c2),
    wf2() OVER(PARTITION BY c1 ORDER BY c2)
FROM table_name;
```
we can use the WINDOW clause to shorten the query as shown in the following
```sql
SELECT 
   wf1() OVER w,
   wf2() OVER w,
FROM table_name
WINDOW w AS (PARTITION BY c1 ORDER BY c2);
```
It is also possible to use the WINDOW clause even though we call one window function in a query.

The following table list all window functions provided by PostgresSQL. Some aggregate functions such as AVG(), MIN(), MAX() can be also used as window functions.

| Name | Description |
| ----- | ----------- |
| CUME_DIST | Return the relative rank of the current row. |
| DENSE_RANK | Rank the current row within its partition without gaps |
| FIRST_VALUE | Return a value evaluated against the first row within its partition |
| LAG | Return a value evaluated at the row that is at a specified physical offset row before the current row within the partition. |
| LAST_VALUE | Return a value evaluated against the last row within its partition. |
| LEAD | Return a value evaluated at the row that is offset rows after the current row within the partition. |
| NTH_VALUE | Return a value evaluated against the nth row in an ordered partition. |
| PERCENT_RANK | Returns the relative rank of the current row (rank-1)/ (total rows -1) |
| RANK | Rank the current row within its partition with gaps. |
| ROW_NUMBER | Number the current row within its partition starting from 1. |