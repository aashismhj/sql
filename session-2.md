## Advance
### 1. Indexes
https://www.enterprisedb.com/postgres-tutorials/overview-postgresql-indexes
- B Tree: 
Default Type, Suitable for most queries, efficient for equality and range queries, Automatically created for primary keys and unique constraints.
- Hash Indexes: Best for equality comparisons. Not suitable for range queries. Use less storage compared to B-Tree indexes but less versatile.
- GIN (generalized Inverted Index) Indexes: 
- GIST (generalized Search Tree) Indexes: 
- SP-GiST (Space-partitioned Generalized Search Tree) Indexes: 
- BRIN (Block Range INdex) Indexes:
- Expression Indexes
- Partial Indexes: Indexes a subset of rows in a table. Useful for indexing frequently queries data subsets.
```sql
-- b-tree
explain analyze select * from log_data where time_taken = 500;
CREATE INDEX index_log_time_taken ON log_data (time_taken);
drop index index_log_time_taken;
-- hash index
explain analyze select * from log_data where country = 'Germany';
CREATE INDEX idx_log_data_country on log_data using hash (country);
drop index idx_log_data_country;
-- partial index
CREATE INDEX index_partial_log_time_taken on log_data(time_taken) where time_taken = 500;
explain analyze select * from log_data where time_taken = 500;
drop index index_partial_log_time_taken;
REINDEX {DATABASE | TABLE | INDEX} name;
```
**Disadvantages**
- Increased storage requirements
- Slower write operations
- HOT Updates: PostgresSQL employs a mechanism called Multi Version Concurrency Control for updates. However, indexes can lead to "HOT" Heap Only Tuples updates. Instead of allowing direct in place updates, each update operation effectively results in a a new row version, generating a new entry in a new row version, generating a new entry in each associated index. This leads to increased I/O activity and the addition of dead rows in the database.

### 2. View
- views are stored queries
- helps in simplified querying, encapsulation
- view can be used to hide columns
```sql
CREATE VIEW log_with_country as select log_data.*, price from log_data left join pricing on log_data.country = pricing.country;
select * from log_with_country where time_taken > 400;
drop view log_with_country;
```

### 3. Material View
- same feature like view
- presistant data unlike view
- can be refreshed
- index can be used
- used with data aggregation and frequently used data
- extra storage
- insert into material view?
```sql
CREATE MATERIALIZED VIEW log_summary AS select log_data.*, price from log_data left join pricing on log_data.country = pricing.country;
-- refreshing material view
REFRESH MATERIALIZED VIEW log_summary;
-- indexing
CREATE INDEX idx_log_summary on log_summary (country);
```
### 4. Stored Procedure
- sql statement that are stored and executed on the database server
- they are precompiled which can offer performance benefits over executing multiple SQL statements individually.
- we can include transaction control statements like BEGIN, COMMIT, ROLLBACK
- stored procedure can accept parameters, enabling dynamic behavior based on input values.
- usage: 
    - create transactional block
- can have complex logic
- procedural language: PL/pgSQL, PL/Tcl, PL/Perl, and PL/Python.
- can declare variables, define return type
```sql
CREATE OR REPLACE PROCEDURE update_log_between(IN start_date DATE, IN end_date DATE)
LANGUAGE plpgsql
AS $$
BEGIN 
	update log_data set geo = 'a' where create_at between start_date and end_date;
END;
$$;
call update_log_between('2020-01-01', '2023-09-01');
--
CREATE OR REPLACE PROCEDURE get_log_between(IN start_date DATE, IN end_date DATE)
LANGUAGE plpgsql
AS $$
BEGIN 
  PERFORM * from log_data where create_at between start_date and end_date;
END;
$$;
call get_log_between('2020-01-01', '2023-01-01');

```
### 5. Function
- function is generally used to return some data
```sql
CREATE OR REPLACE FUNCTION example_function()
RETURNS VOID AS $$
DECLARE
    start_time TIMESTAMP;
    end_time TIMESTAMP;
BEGIN
    -- Record the start time
    start_time := clock_timestamp();

    -- Execute the query using PERFORM
    select * FROM log_data WHERE time_taken > 500;

    -- Record the end time
    end_time := clock_timestamp();

    -- Calculate and print the execution time
    RAISE NOTICE 'Query execution time: %', end_time - start_time;
END;
$$ LANGUAGE plpgsql;
select example_function();
CREATE OR REPLACE FUNCTION get_time_taken()
RETURNS text AS $$
DECLARE
    start_time TIMESTAMP;
    end_time TIMESTAMP;
BEGIN
    -- Record the start time
    start_time := clock_timestamp();

    -- Execute the query using PERFORM
    PERFORM * FROM log_data WHERE time_taken > 500;

    -- Record the end time
    end_time := clock_timestamp();

    -- Calculate and print the execution time
    RAISE NOTICE 'Query execution time: %', end_time - start_time;
	return end_time - start_time;
END;
$$ LANGUAGE plpgsql;
select *, get_time_taken() from log_data limit 10;

drop function get_log_col;
create or replace function get_log_col (
  time_value int
) 
returns table (
	cnt varchar,
	tt int
) 
language plpgsql
as $$
begin
	return query 
		select
			country as cnt,
			time_taken as tt
-- 			release_year::integer
		from
			log_data
		where
			time_taken > time_value;
end;
$$;

select * from get_log_col(500) limit 10;
```
### 6. Trigger
- function that run on event
- row level and statement level
- can have before and after
- beware of infinite loop
- can modify data using before?
```sql
-- syntax to create trigger
CREATE TRIGGER trigger_name 
   {BEFORE | AFTER} { event }
   ON table_name
   [FOR [EACH] { ROW | STATEMENT }]
       EXECUTE PROCEDURE trigger_function
-- 
CREATE OR REPLACE FUNCTION log_salary_change()
RETURNS TRIGGER 
AS 
$$
BEGIN
    INSERT INTO salary_changes (employee_id, old_salary, new_salary)
    VALUES (NEW.id, OLD.salary, NEW.salary);
    
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER salary_change
  BEFORE UPDATE
  ON employees
  FOR EACH ROW
  EXECUTE PROCEDURE log_salary_change();
```

**Some Queries**
```sql
drop table log_data;
drop table org_users;
drop table org;
drop table partner_tier;
drop table pricing;
```

## Tasks
- count the no of logs a users has
```sql
-- using sub query

select id, first_name, last_name, ref_code, (select count(*) from log_data where org_users.ref_code = log_data.ref_code) as count 
from org_users order by first_name, last_name limit 5;
-- using join and group by
select first_name, last_name, ref_code, count(log_data.*) from org_users
right join log_data using(ref_code) group by ref_code, first_name, last_name order by first_name, last_name;
-- using CTE
WITH LogCount AS (
	select count(*), ref_code from log_data group by ref_code
)
select id, first_name, last_name, LogCount.ref_code, count from org_users left join LogCount on LogCount.ref_code = org_users.ref_code limit 5;

```

- count the no of logs a company has
- Show all the users who have the time_taken greater then 900, and also show the count of how many log they have greater then 900
- show all users where are affilated to top tier and has logs more than 10;
- show the date the org had their highest count

```
// Use DBML to define your database structure
// Docs: https://dbml.dbdiagram.io/docs

Table partner_tier {
  id integer [primary key]
  tier_name varchar
  percentage int
}

Table org {
  id integer [primary key]
  name varchar
  country varchar
  partner_tier timestamp
}

Table org_users {
  id integer [primary key]
  first_name varchar
  last_name varchar
  email text [note: 'User Email']
  ref_code integer
  status varchar
  phone_no varchar
  reports_to int
  org_id int
}

Table pricing {
  id int [primary key]
  country varchar
  price int
}

Table log_data {
  create_at date
  created_time time
  updated_at date
  update_time time
  country varchar
  time_taken int
  ref_code varchar
  geo varchar
}
Ref: partner_tier.id < org.partner_tier
Ref: org.id < org_users.org_id
Ref: org_users.id < org_users.reports_to
Ref: org_users.ref_code < log_data.ref_code

```